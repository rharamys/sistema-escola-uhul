package com.itau.escolauhul.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.escolauhul.models.Professor;
import com.itau.escolauhul.repositories.ProfessorRepository;


@RestController
public class ProfessorController {

	@Autowired
	ProfessorRepository professorRepository;

	// Um professor especifico
	@RequestMapping(method=RequestMethod.GET, path="/professor/{id}")
	public ResponseEntity<Professor> getProfessor(@PathVariable long id) {
		Optional<Professor> optionalProfessor = professorRepository.findById(id);
		if (!optionalProfessor.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		return ResponseEntity.ok().body(optionalProfessor.get());
	}
		


	//	 Todos os professores
	@RequestMapping(method=RequestMethod.GET, path="/professores")
	public ResponseEntity<Iterable<Professor>> getProfessores() {

		return ResponseEntity.ok().body(professorRepository.findAll());
	}
	
//	Inserir Professor
	@RequestMapping(method=RequestMethod.POST, path="/professor")
	public ResponseEntity<Professor> inserirProfessor(@Valid @RequestBody Professor professor) {
	
		try {
			professorRepository.save(professor);
			
			return ResponseEntity.ok().body(professor);
			
		} catch (Exception e) {

			return ResponseEntity.badRequest().build();
		}
		
	}
		
	
}
