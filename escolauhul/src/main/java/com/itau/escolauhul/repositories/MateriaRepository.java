package com.itau.escolauhul.repositories;

import org.springframework.data.repository.CrudRepository;
import com.itau.escolauhul.models.Materia;

public interface MateriaRepository extends CrudRepository<Materia, String> {

}
