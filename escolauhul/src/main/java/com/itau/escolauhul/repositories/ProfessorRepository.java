package com.itau.escolauhul.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.escolauhul.models.*;

public interface ProfessorRepository extends CrudRepository<Professor, Long> {

	
}
